﻿using System;

namespace Utils
{
	public static class EnumUtil
	{
		public static E GetEnumFromString<E>(string enumName) where E: Enum
		{
			return (E)Enum.Parse(typeof(E), enumName);
		}
	}
}
