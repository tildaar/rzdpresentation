using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Utils
{
    public class FPSCounter : MonoBehaviour
    {
        private const float FPS_MEASURE_PERIOD = 0.5f;
        private const string DISPLAY = "{0} FPS";

        private TextMeshProUGUI mText;
        
        private int mFpsAccumulator = 0;
        private float mFpsNextPeriod = 0;
        private int mCurrentFps;

        private void Start()
        {
            mFpsNextPeriod = Time.realtimeSinceStartup + FPS_MEASURE_PERIOD;
            mText = GetComponent<TextMeshProUGUI>();
        }

        private void Update()
        {
            mFpsAccumulator++;
            
            if (!(Time.realtimeSinceStartup > mFpsNextPeriod)) 
                return;
            
            mCurrentFps = (int) (mFpsAccumulator/FPS_MEASURE_PERIOD);
            mFpsAccumulator = 0;
            mFpsNextPeriod += FPS_MEASURE_PERIOD;
            mText.text = string.Format(DISPLAY, mCurrentFps);
        }
    }
}
