﻿using System;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Serialization;
using AnimatorController = UnityEditor.Animations.AnimatorController;

public class FbxAutomationWindow : EditorWindow
{
    [FormerlySerializedAs("Models")] [SerializeField]
    private GameObject[] _models;
    
    [MenuItem("RZD/Fbx Automation")]
    private static FbxAutomationWindow OpenFbxAutomationWindow()
    {
        var editorWindow = EditorWindow.GetWindow<FbxAutomationWindow>("Fbx Automation Window");
        editorWindow.minSize = new Vector2(380, 200);
        return editorWindow;
    }

    private void OnGUI()
    {
        var serialObj = new SerializedObject(this);
        
        var winRect = new Rect(10, 20, 360, 300);
        GUILayout.BeginArea(winRect);

        EditorGUILayout.BeginVertical();
        
        EditorGUILayout.HelpBox("ПЕРЕТАЩИТЕ FBX МОДЕЛИ ИЗ ИЕРАРХИИ СЦЕНЫ", MessageType.Info, true);
        
        var modelsProp = serialObj.FindProperty("_models");
        EditorGUILayout.PropertyField(modelsProp, new GUIContent(""));
        serialObj.ApplyModifiedProperties();
        
        EditorGUILayout.Space(10);

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.Space(0);
        if(GUILayout.Button("Set Animators",
            new GUILayoutOption[] { GUILayout.Height(30) }))
            Start();

        if(GUILayout.Button("Clear", new GUILayoutOption[] { GUILayout.Height(30) }))
            ClearModels();
        EditorGUILayout.Space(0);
        EditorGUILayout.EndHorizontal();
        
        EditorGUILayout.EndVertical();
        
        GUILayout.EndArea();

        if (_models == null) 
            return;

        if (_models.Where(m => m != null)
            .All(m => PrefabUtility.GetCorrespondingObjectFromSource<GameObject>(m) != null))
            return;
        
        _models = null;
        throw new Exception("Модель должна находиться в иерархии сцены");
    }

    private void Start()
    {
        ClearConsole();

        if (_models == null)
            throw new Exception("model is undefined!");

        foreach (var model in _models)
        {
            var fbxSrc = PrefabUtility.GetCorrespondingObjectFromOriginalSource(model);
            var assetPath = AssetDatabase.GetAssetPath(fbxSrc);
            
            Debug.Log($"assetPath > {assetPath}");
            Debug.Log($"model > {model}");
            //Debug.Log($"model is part of prefab > {PrefabUtility.IsPartOfPrefabAsset(model)}");

            var animator = model.GetComponent<Animator>();
            if(animator == null)
                animator = model.AddComponent<Animator>();
        
            var tmp = assetPath.Split('/').ToList();
            tmp.Remove(tmp.Last());
            var finalPath = string.Join("/", tmp);
            Debug.Log($"model > {finalPath}");
        
            var controller = AnimatorController.CreateAnimatorControllerAtPath(finalPath + $"/{model.name}_animator.controller");
            animator.runtimeAnimatorController = controller;
        
            var rootStateMachine = controller.layers[0].stateMachine;
            rootStateMachine.AddState("idle");
            var openState = rootStateMachine.AddState("open");
            var closeState = rootStateMachine.AddState("close");
        
            var allAssets = AssetDatabase.LoadAllAssetsAtPath(assetPath);
            var openClip = allAssets.First(o => o is AnimationClip && o.name == "open") as AnimationClip;
            var closeClip = allAssets.First(o => o is AnimationClip && o.name == "close") as AnimationClip;
        
            openState.motion = openClip;
            closeState.motion = closeClip;
        }
    }
    
    private void ClearModels()
    {
        _models = null;
    }
    
    private static void ClearConsole()
    {
        var logEntries = System.Type.GetType("UnityEditor.LogEntries, UnityEditor.dll");
        var clearMethod = logEntries.GetMethod("Clear",
            System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public);
        clearMethod.Invoke(null, null);
    }
}
