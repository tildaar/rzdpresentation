﻿using Events;
using System;
using UnityEngine;

using VRTK.Prefabs.Pointers;
using Zenject;
using Zinnia.Action;
using Zinnia.Pointer;

using  static AppEnums;

namespace Pointers
{
	public class VRPointerController : MonoBehaviour
	{
		[Inject] private ApplicationManager _appManager;

		private PointerFacade _pointerFacade = default;
		private GameObject _currentInteractableTarget = default;
		private GameObject _currentTeleportTarget = default;

		public void Initialize(PointerFacade pointerFacade)
		{
			_pointerFacade = pointerFacade;
			//Debug.Log($"app manager | VRPointerController > {AppManager}");
			_pointerFacade.Activated.AddListener(ActivatedHandler);
			_pointerFacade.Deactivated.AddListener(DeactivatedHandler);
			_pointerFacade.Entered.AddListener(EnteredChangedHandler);
			_pointerFacade.Exited.AddListener(ExitedChangedHandler);
		}

		private void OnDestroy()
		{
			_pointerFacade.Activated.RemoveListener(ActivatedHandler);
			_pointerFacade.Deactivated.RemoveListener(DeactivatedHandler);
			_pointerFacade.Entered.RemoveListener(EnteredChangedHandler);
			_pointerFacade.Exited.RemoveListener(ExitedChangedHandler);
		}

		private void EnteredChangedHandler(ObjectPointer.EventData data)
		{
			if (_appManager.ViewMode == ViewModes.VR && !_pointerFacade.ActivationAction.Value)
				return;

			var collider = data?.CollisionData.collider;
			if (collider?.gameObject.layer == Layers.InteractableLayer.GetHashCode())
			{
				_currentInteractableTarget = collider.gameObject;
				Messenger.Broadcast(ApplicationEvents.POINTER_ENTER, _currentInteractableTarget);
			}
			else if (collider?.gameObject.layer == Layers.TeleportLayer.GetHashCode())
			{
				_currentTeleportTarget = collider.gameObject;
				Messenger.Broadcast(ApplicationEvents.TELEPORT_POINTER_ENTER, _currentTeleportTarget);
			}
		}

		private void ExitedChangedHandler(ObjectPointer.EventData data)
		{
			if (_appManager.ViewMode == ViewModes.VR && !_pointerFacade.ActivationAction.Value)
				return;

			var collider = data?.CollisionData.collider;
			if (collider?.gameObject.layer == Layers.InteractableLayer.GetHashCode())
			{
				Messenger.Broadcast(ApplicationEvents.POINTER_EXIT, _currentInteractableTarget);
				_currentInteractableTarget = null;
			}
			else if (collider?.gameObject.layer == Layers.TeleportLayer.GetHashCode())
			{
				_currentTeleportTarget = collider.gameObject;
				if (_currentTeleportTarget == null) 
					return;
				Messenger.Broadcast(ApplicationEvents.TELEPORT_POINTER_EXIT, _currentTeleportTarget);
				_currentTeleportTarget = null;
			}
		}

		private void ActivatedHandler(ObjectPointer.EventData data)
		{
			var collider = data?.CollisionData.collider;

			if (collider?.gameObject.layer == Layers.InteractableLayer.GetHashCode())
			{
				_currentInteractableTarget = collider.gameObject;
				Messenger.Broadcast(ApplicationEvents.POINTER_ACTION, _currentInteractableTarget);
			}
			else if (collider?.gameObject.layer == Layers.TeleportLayer.GetHashCode())
			{
				_currentTeleportTarget = collider.gameObject;
				Messenger.Broadcast(ApplicationEvents.TELEPORT_POINTER_ACTION, _currentTeleportTarget);
			}
		}

		private void DeactivatedHandler(ObjectPointer.EventData data)
		{
			if (_currentInteractableTarget != null)
			{
				//Debug.Log($"selected > {_currentInteractableTarget}");
				_currentInteractableTarget = null;
			}
			
			if (_currentTeleportTarget != null)
				_currentTeleportTarget = null;
		}
	}
}