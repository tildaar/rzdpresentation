﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Pointers
{
	public interface IPointersHandler
	{
		void AddPointersListeners();
		void RemovePointersListeners();
		void PointerActionHandler(GameObject target);
		void PointerEnterHandler(GameObject target);
		void PointerExitHandler(GameObject target);
	}
}


