﻿#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;

using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayProject : MonoBehaviour
{
    [MenuItem("RZD/PlayProject &p")]
    public static void Play()
    {
        if (EditorApplication.isPlaying)
            return;

        EditorApplication.ExecuteMenuItem("File/Save Project");
        string scenesToClose = "";

        if (SceneManager.sceneCount > 1)
        {
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                var scene = SceneManager.GetSceneAt(i);
                EditorSceneManager.SaveScene(scene);

                if (i > 0)
                {
                    scenesToClose += scene.path + (i == SceneManager.sceneCount - 1 ? "" : ",");
                    EditorSceneManager.CloseScene(scene, true);
                    i--;
                }
            }
        }

        if (!string.IsNullOrEmpty(scenesToClose))
        {
            PlayerPrefs.SetString("scenesToOpen", scenesToClose.ToString());
            //Log.Print($"scenesToClose > {scenesToClose}");
        }

        EditorApplication.isPlaying = true;
    }

    [MenuItem("RZD/StopProject _END")]
    public static void Stop()
    {
        if (!EditorApplication.isPlaying)
            return;

        EditorApplication.isPlaying = false;
        EditorApplication.playModeStateChanged += PlayModeStateChangedHandler;
     }

    private static void PlayModeStateChangedHandler(PlayModeStateChange state)
    {
        EditorApplication.playModeStateChanged -= PlayModeStateChangedHandler;

        if (state == PlayModeStateChange.EnteredEditMode)
        {
            var scenesToOpen = PlayerPrefs.GetString("scenesToOpen").Split(new char[] { ',' });
            if (scenesToOpen != null)
            {
                for (int i = 0; i < scenesToOpen.Length; i++)
                {
                    EditorSceneManager.OpenScene(scenesToOpen[i], OpenSceneMode.Additive);
                }
            }
        }
    }
}
#endif
