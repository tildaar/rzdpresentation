﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;
using  static AppEnums;

public class AppModeSelector : MonoBehaviour
{
    [Inject]
    private readonly ApplicationManager _appManager = default;

    [SerializeField]
    private Button _screenModeButton = default;

    [SerializeField]
    private Button _vrModeButton = default;

    // TEMP
    [SerializeField]
    private Button _startTourButton = default;

    private void Start()
    {
       // Debug.Log($"app manager > {AppManager}");
        _screenModeButton.onClick.AddListener(SelectScreenModeHandler);
        _vrModeButton.onClick.AddListener(SelectVRModeHandler);
        // TEMP
        //_startTourButton.onClick.AddListener(StartTourButtonHandler);
    }

    private void Destroy()
    {
        _screenModeButton.onClick.RemoveListener(SelectScreenModeHandler);
        _vrModeButton.onClick.RemoveListener(SelectVRModeHandler);
        //TEMP
        //_startTourButton.onClick.RemoveListener(StartTourButtonHandler);
    }

    //TEMP
    //private void StartTourButtonHandler()
    //{
    //    AppManager.OnRailwayCarSelected?.Invoke(WagonsSections.Test);
    //}

    private static void SelectVRModeHandler()
    {
        //AppManager.OnViewModeSelected?.Invoke(ViewModes.VR);
    }

    private void SelectScreenModeHandler()
    {
        _appManager.OnViewModeSelected?.Invoke(ViewModes.Screen);
    }
}
