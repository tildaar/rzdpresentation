﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ScenesPreloader : MonoBehaviour
{
	[SerializeField]
	private RectTransform _progressTransform = default;

	[SerializeField]
	private CanvasGroup _bground = default;

	private int _fadeTweenId;

	private void Start()
	{
		_bground.alpha = 0;
		_progressTransform.gameObject.SetActive(false);

		_fadeTweenId = LeanTween.alphaCanvas(_bground, 1, 1)
			.setOnComplete(() =>
			{
				_progressTransform.gameObject.SetActive(true);
				LeanTween.rotateAroundLocal(_progressTransform, Vector3.forward, 360, 1)
					.setRepeat(-1).
					setEaseLinear();
			})
			.id;
	}

	private void OnDestroy()
	{
		LeanTween.cancel(_progressTransform);
		LeanTween.cancel(_fadeTweenId);
	}
}

