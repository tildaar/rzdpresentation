﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

using Pointers;

using UnityEngine;
using UnityEngine.UI;

using BaseMenu;
using Events;
using Zenject;

using static AppEnums;

namespace Slides
{
	public class SlidesController : MonoBehaviour, IPointersHandler
	{
		[Inject] private MenuSceneInitializer _menuSceneInitializer = default;
		[Inject] private ApplicationManager _appManager = default;

		[SerializeField] private RawImage _imageHolder = default;

		#region Hit Areas
		[Header("Hit Areas")]
		[SerializeField] private Image _prevBtnIcon = default;
		[SerializeField] private Image _nextBtnIcon = default;
		[SerializeField] private Image _closeBtnIcon = default;
		
		[SerializeField] private Image _prevHitArea = default;
		[SerializeField] private Image _nextHitArea = default;
		#endregion
		
		[SerializeField] [Space(5)] private CanvasGroup _logos = default;
		
		//private Dictionary<string, Texture2D> _texturesHash = new Dictionary<string, Texture2D>();
		private List<string> _imgPaths;

		private int _currentIndex = 0;
		private int _maxIndex;

		#region Unity Native

		private void Start()
		{
			_imageHolder.transform.localScale = new Vector3(0, 0);
			_imgPaths = GetImagesPaths();
			_maxIndex = _imgPaths.Count - 1;
			
			Debug.Log($"appManager | SledesController > {_appManager}");

			_appManager.CloseSlidesWithKeyboard += SlidesViewerCloseHandler;
			
			AddPointersListeners();
			HideGUIElements();
			ShowImage();
		}

		private void OnDestroy()
		{
			Destroy(_imageHolder.mainTexture);
			_appManager.CloseSlidesWithKeyboard -= SlidesViewerCloseHandler;
			//foreach (var paar in _texturesHash)
			//	Destroy(paar.Value);
			RemovePointersListeners();
		}
		#endregion

		private void HideGUIElements()
		{
			LeanTween.alpha(_prevBtnIcon.rectTransform, 0, 0);
			LeanTween.alpha(_nextBtnIcon.rectTransform, 0, 0);
			LeanTween.alpha(_closeBtnIcon.rectTransform, 0, 0);
			
			LeanTween.alphaCanvas(_logos, 0, 0);
		}

		private void ShowGUIElements()
		{
			LeanTween.alpha(_prevBtnIcon.rectTransform, 0.1f, 0.4f);
			_prevHitArea.raycastTarget = false;
			
			LeanTween.alpha(_nextBtnIcon.rectTransform, 1, 0.4f);
			LeanTween.alpha(_closeBtnIcon.rectTransform, 1, 0.4f);
			LeanTween.alphaCanvas(_logos, 1, 0.4f);
		}

		private static void ToggleBtnInteractable(Graphic hitArea, Graphic icon, bool value)
		{
			icon.color = value ? new Color(1, 1, 1, 1) : new Color(1, 1, 1, 0.1f);
			hitArea.raycastTarget = value;
		}

		private void FadeImageOut()
		{
			_prevHitArea.raycastTarget = _nextHitArea.raycastTarget = false;
			LeanTween.color(_imageHolder.rectTransform, new Color32(0, 0, 0, 255), .5f)
				.setOnComplete(ShowImage);
		}

		private async void ShowImage()
		{
			var path = _imgPaths[_currentIndex];
			Texture2D tex = null;

			//if (_texturesHash.ContainsKey(path))
			//{
			//	tex = _texturesHash[path];
			//}
			//else
			//{
			byte[] bytes;
			using (var sourceStream = File.Open(path, FileMode.Open))
			{
				bytes = new byte[sourceStream.Length];
				await sourceStream.ReadAsync(bytes, 0, (int)sourceStream.Length);
			}

			tex = new Texture2D(1, 1);
			tex.LoadImage(bytes, true);
			//_texturesHash.Add(path, tex);
			//}
			
			Destroy(_imageHolder.texture);
			_imageHolder.texture = tex;

			LeanTween.color(_imageHolder.rectTransform, new Color32(255, 255, 255, 255), .5f)
				.setOnComplete(() =>
				{
					if (_currentIndex == 0)
					{
						_nextHitArea.raycastTarget = true;
						_prevHitArea.raycastTarget = false;
					}
					else if (_currentIndex == _maxIndex)
					{
						_nextHitArea.raycastTarget = false;
						_prevHitArea.raycastTarget = true;
					}
					else
						_nextHitArea.raycastTarget = _prevHitArea.raycastTarget = true;
				});

			if (_imageHolder.transform.localScale.Equals(Vector3.zero))
			{
				LeanTween.scale(_imageHolder.gameObject, new Vector3(1, 1), .6f)
					.setEaseOutSine()
					.setOnComplete(ShowGUIElements);
			}
		}

		private List<string> GetImagesPaths()
		{
			var path = Application.streamingAssetsPath + Path.DirectorySeparatorChar + _menuSceneInitializer.CurrentSlidersSet.ToString();

			var allFiles = Directory.GetFiles(path);
			var paths = new List<string>();

			//Debug.Log($" --------- {this} ------------");
			foreach (var imgPath in allFiles)
			{
				if (imgPath.Split(new char[] { '.' }).Last() == "meta")
					continue;

				paths.Add(imgPath);
			}

			//Debug.Log($" -----------------------------");
			return paths;
		}

		public void AddPointersListeners()
		{
			Messenger.AddListener<GameObject>(ApplicationEvents.POINTER_ACTION, PointerActionHandler);
			Messenger.AddListener<GameObject>(ApplicationEvents.POINTER_ENTER, PointerEnterHandler);
			Messenger.AddListener<GameObject>(ApplicationEvents.POINTER_EXIT, PointerExitHandler);
		}

		public void RemovePointersListeners()
		{
			if (!Messenger.eventTable.ContainsKey(ApplicationEvents.POINTER_ACTION)) 
				return;
			Messenger.RemoveListener<GameObject>(ApplicationEvents.POINTER_ACTION, PointerActionHandler);
			Messenger.RemoveListener<GameObject>(ApplicationEvents.POINTER_ENTER, PointerEnterHandler);
			Messenger.RemoveListener<GameObject>(ApplicationEvents.POINTER_EXIT, PointerExitHandler);
		}

		public void PointerActionHandler(GameObject target)
		{
			var targetType = Utils.EnumUtil.GetEnumFromString<ApplicationTags>(target.tag);
			switch (targetType)
			{
				case ApplicationTags.SlidesViewerCloseBtn: CloseBtnHandler(); break;
				case ApplicationTags.SlidesViewerPrevBtn: PrevBtnHandler(); break;
				case ApplicationTags.SlidesViewerNextBtn: NextBtnHandler(); break;
				case ApplicationTags.ToWagonTourBtn:
				case ApplicationTags.ToWagonSlidesBtn:
				case ApplicationTags.Untagged:
				default: break;
			}
		}

		public void PointerEnterHandler(GameObject target)
		{
			//throw new NotImplementedException();
		}

		public void PointerExitHandler(GameObject target)
		{
			//throw new NotImplementedException();
		}
		
		private void NextBtnHandler()
		{
			//Debug.Log($"_prevHitArea.raycastTarget = {_prevHitArea.raycastTarget}");
			if (!_prevHitArea.raycastTarget)
				ToggleBtnInteractable(_prevHitArea, _prevBtnIcon, true);

			if (_currentIndex < _maxIndex)
			{
				_currentIndex++;
				FadeImageOut();
			}

			if(_currentIndex == _maxIndex)
				ToggleBtnInteractable(_nextHitArea, _nextBtnIcon, false);
		}

		private void PrevBtnHandler()
		{
			if (!_nextHitArea.raycastTarget)
				ToggleBtnInteractable(_nextHitArea, _nextBtnIcon, true);

			if (_currentIndex > 0)
			{
				_currentIndex--;
				FadeImageOut();
			}

			if(_currentIndex == 0)
				ToggleBtnInteractable(_prevHitArea, _prevBtnIcon, false);
		}
		
		private void CloseBtnHandler()
		{
			LeanTween.alpha(_prevBtnIcon.rectTransform, 0, 0.2f);
			LeanTween.alpha(_nextBtnIcon.rectTransform, 0, 0.2f);
			LeanTween.alpha(_closeBtnIcon.rectTransform, 0, 0.2f);
			
			LeanTween.scale(_imageHolder.gameObject, new Vector3(0, 0), .6f)
				.setEaseOutSine()
				.setOnComplete(() =>
				{
					_appManager.OnSlidesViewerClose?.Invoke();
				});
		}
		
		private void SlidesViewerCloseHandler()
		{
			CloseBtnHandler();
		}
	}
}
