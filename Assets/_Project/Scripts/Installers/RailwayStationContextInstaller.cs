using System;
using Pointers;
using Tours.Triggers;
using UnityEngine;
using Zenject;

public class RailwayStationContextInstaller : MonoInstaller
{
    [SerializeField] private VRPointerController _vrPointerController;

    [SerializeField] private Prefabs _prefabs;
    
    public override void InstallBindings()
    {
        Container.Bind<TriggersController>().AsSingle();
        Container.BindInstance(_vrPointerController).AsSingle();
        Container.BindInstance<Prefabs>(_prefabs);
    }
    
    [Serializable]
    public class Prefabs
    {
        [SerializeField] private GameObject _vRTKScreenModePrefab;
        public GameObject VRTKScreenModePrefab => _vRTKScreenModePrefab;
        
        [SerializeField] private GameObject _fpsCounterPrefab;
        public GameObject FpsCounterPrefab => _fpsCounterPrefab;
    }
}