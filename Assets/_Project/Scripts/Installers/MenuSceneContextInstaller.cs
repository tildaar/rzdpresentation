using System;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "MenuSceneContextInstaller", menuName = "Installers/MenuSceneContextInstaller")]
public class MenuSceneContextInstaller : ScriptableObjectInstaller<MenuSceneContextInstaller>
{
    [SerializeField] private Prefabs _prefabs;
    
    public override void InstallBindings()
    {
        Container.BindInstance<Prefabs>(_prefabs);
    }
    
    [Serializable] public class Prefabs
    {
       [SerializeField] private GameObject _slidesViewer;
       public GameObject SlidesViewer => _slidesViewer;
    }
}