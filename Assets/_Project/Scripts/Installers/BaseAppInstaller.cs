﻿using System;
using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "BaseAppInstaller", menuName = "Installers/BaseAppInstaller")]
public class BaseAppInstaller : ScriptableObjectInstaller<BaseAppInstaller>
{
	[SerializeField] private Prefabs _prefabs = default;

	public override void InstallBindings()
    {
		Container.BindInstance<Prefabs>(_prefabs);
    }

	[Serializable] public class Prefabs
	{
		public GameObject ScenePreloader;
		public GameObject FPSCounter;

		[Header("Вагоны")] [SerializeField] private Wagons _wagons;

		internal Wagons WagonsPrefabs => _wagons;

		[Serializable] public class Wagons
		{
			[Serializable] public class WagonsDictionary : SerializableDictionary<AppEnums.WagonsTypes, GameObject>{ }
			
			[SerializeField] private WagonsDictionary _ = default;

			public GameObject Get(AppEnums.WagonsTypes type)
			{
				_.TryGetValue(type, out var prefab);
				return prefab;
			}
		}
	}
}