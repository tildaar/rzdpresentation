﻿using System;
using Tours;
using UnityEngine;

namespace Tours
{
    public class WagonInitializer : MonoBehaviour
    {
        [SerializeField] private TeleportPoint _startTeleportPoint;
        public TeleportPoint StartTeleportPoint => _startTeleportPoint;

    }
}