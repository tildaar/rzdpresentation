﻿using Controllers.Timelines;
using JetBrains.Annotations;
using Tours.Triggers;
using UnityEngine.Playables;

namespace Tours.TargetPresenters
{
    public class AlembicPresenter : AbstractTargetPresenter
    {
        private readonly TimelineController _timelineController;

        private readonly float _directAnimationStart;
        private readonly float _directAnimationEnd;
        private readonly float _reverseAnimationStart;
        private readonly float _reverseAnimationEnd;

        private AnimationStates _state = AnimationStates.Close;
        //private AnimationStates _state = AnimationStates.Open;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="trigger"></param>
        public AlembicPresenter(ITriggerController trigger) : base(trigger)
        {
            var playableDirector = _trigger.Target.GetComponent<PlayableDirector>();
            _timelineController = new TimelineController(playableDirector);

            _directAnimationStart = _trigger.DirectAnimationTimeRange.Start;
            _directAnimationEnd = _trigger.DirectAnimationTimeRange.End;
            
            _reverseAnimationStart = _trigger.ReverseAnimationTimeRange.Start;
            _reverseAnimationEnd = _trigger.ReverseAnimationTimeRange.End;
        }

        public override void Present()
        {
            base.Present();
            
            if (_timelineController.PlayState == PlayState.Playing)
                return;

            if (_state == AnimationStates.Close)
            {
                _timelineController.PlayRegion(_directAnimationStart, _directAnimationEnd, 
                    () => _state = AnimationStates.Open);
            }
            else
            {
                _timelineController.PlayRegion(_reverseAnimationStart, _reverseAnimationEnd, 
                    () => _state = AnimationStates.Close);
            }
        }
    }
}