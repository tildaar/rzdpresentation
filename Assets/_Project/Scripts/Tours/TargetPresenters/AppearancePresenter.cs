﻿using Tours.Triggers;
using UnityEngine;

namespace Tours.TargetPresenters
{
    public class AppearancePresenter : AbstractTargetPresenter
    {
        private enum Modes
        {
            Animation,
            Program
        }
        
        private readonly Animator _animator;
        private bool _isShown;
        private Modes _mode;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="trigger"></param>
        public AppearancePresenter(ITriggerController trigger) : base(trigger)
        {
            _animator = _trigger.Target.GetComponent<Animator>();
            _mode = _animator == null ? Modes.Program : Modes.Animation;
        }

        public override void Present()
        {
            base.Present();
            
            if (_isShown)
                return;
            
            if (_mode == Modes.Program)
            {
                //_trigger.Target.transform.localScale = Vector3.zero;
                LeanTween.scale(_trigger.Target, new Vector3(1, 1, 1), 0.2f)
                    .setEaseOutSine();
            }
            else
            {
                var open = AnimationStates.Open.ToString().ToLower();
                _animator.Play(open);
            }
            _isShown = true;
        }
    }
}