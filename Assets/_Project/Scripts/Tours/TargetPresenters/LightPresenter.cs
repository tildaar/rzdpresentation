﻿using Tours;
using Tours.Triggers;
using UnityEngine;

namespace Tours.TargetPresenters
{
    public class LightPresenter : AbstractTargetPresenter
    {
        private readonly Light _light;
        private Material[] _materials;

        public LightPresenter(ITriggerController trigger) : base(trigger)
        {
            _light = _trigger.Target.GetComponent<Light>();
            
            if(!(_trigger is TriggerController))
                return;

            _materials = (_trigger as TriggerController).LampGlassRenderer.materials;
        }

        public override void Present()
        {
            base.Present();
            
            _light.enabled = !_light.enabled;
            foreach (var m in _materials)
                m.SetFloat(TriggersController.EmissiveExposureWeight, _light.enabled ? 0 : 1);
        }
    }
}