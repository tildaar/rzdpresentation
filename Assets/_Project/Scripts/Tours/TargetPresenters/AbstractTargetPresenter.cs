﻿using System.Collections;
using Tours.Triggers;
using UnityEngine;

namespace Tours.TargetPresenters
{
    public abstract class AbstractTargetPresenter
    {
        protected enum AnimationStates
        {
            Idle,
            Open,
            Close
        }

        public bool IsPlaying { get; protected set; }
        
        protected readonly ITriggerController _trigger;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="trigger"></param>
        protected AbstractTargetPresenter(ITriggerController trigger)
        {
            _trigger = trigger;
        }

        public virtual async void Present() { }

    }
}