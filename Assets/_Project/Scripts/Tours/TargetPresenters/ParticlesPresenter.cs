﻿using Tours.Triggers;
using UnityEngine.VFX;

namespace Tours.TargetPresenters
{
    public class ParticlesPresenter : AbstractTargetPresenter
    {
        private readonly VisualEffect _particles;
        
        public ParticlesPresenter(ITriggerController trigger) : base(trigger)
        {
           _particles = _trigger.Target.GetComponent<VisualEffect>();
        }

        public override void Present()
        {
            base.Present();

            if (!IsPlaying)
                _particles.Play();
            else
                _particles.Stop();

            IsPlaying = !IsPlaying;
        }
    }
}