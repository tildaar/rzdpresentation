﻿using System;
using Tours.Triggers;
using UnityEngine;

namespace Tours.TargetPresenters
{
    public class AlembicTemp : MonoBehaviour
    {
        private ITriggerController _triggerController;

        private void Start()
        {
            _triggerController = GetComponent<ITriggerController>();
            var presenter = new AlembicPresenter(_triggerController);
            presenter.Present();
            TriggersController.AddCurrentTrigger(_triggerController, presenter);
        }
    }
}