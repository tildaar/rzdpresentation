﻿using System;
using System.Threading.Tasks;
using Tours.Triggers;
using UnityEngine;

namespace Tours.TargetPresenters
{
    public class FbxPresenter : AbstractTargetPresenter
    {
        private readonly Animator _animator;
        private readonly bool _isOnlyOpenAnimation;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="trigger"></param>
        public FbxPresenter(ITriggerController trigger) : base(trigger)
        {
            _animator = _trigger.Target.GetComponent<Animator>();
            _isOnlyOpenAnimation = _animator.runtimeAnimatorController.animationClips.Length == 1;
        }

        public override async void Present()
        {
            base.Present();

            if (IsPlaying)
                return;

            var stateInfo = _animator.GetCurrentAnimatorStateInfo(0);
            
            var open = AnimationStates.Open.ToString().ToLower();
            var idle = AnimationStates.Idle.ToString().ToLower();
            var isIdle = stateInfo.IsName(idle);
            var isOpen = stateInfo.IsName(open);

            if (_isOnlyOpenAnimation)
            {
                if(isIdle || isOpen)
                    _animator.Play(open);
                
                //Debug.Log($"stateInfo isIdle > {isIdle}, isOpen > {isOpen}, duration > {stateInfo.length}, IsPlaying > {IsPlaying}");
                IsPlaying = true;
                await Task.Delay(TimeSpan.FromSeconds(stateInfo.length));
                //Debug.Log($"stateInfo isIdle > {isIdle}, isOpen > {isOpen}, duration > {stateInfo.length}, IsPlaying > {IsPlaying}");
                IsPlaying = false;
                
                return;
            }
            
            var close = AnimationStates.Close.ToString().ToLower();
            var isClose = stateInfo.IsName(close);

            //Debug.Log($"animationClips length > {_animator.runtimeAnimatorController.animationClips.Length}");
            //Debug.Log($"animator gameobject > {_animator.runtimeAnimatorController}");
            //Debug.Log($"isIdle > {isIdle}, isOpen > {isOpen}, isClose > {isClose}, duration > {duration}");
            
            if (isIdle || (!isOpen && isClose))
                _animator.Play(open);

            if (isOpen && !isClose) 
               _animator.Play(close);

            IsPlaying = true;
            await Task.Delay(TimeSpan.FromSeconds(stateInfo.length));
            IsPlaying = false;
        }
    }
}