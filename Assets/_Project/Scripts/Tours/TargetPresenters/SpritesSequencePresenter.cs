﻿using System;
using System.Threading.Tasks;
using Tours.Triggers;
using UnityEngine;

namespace Tours.TargetPresenters
{
    public class SpritesSequencePresenter : AbstractTargetPresenter
    {
        private readonly Animator _animator;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="trigger"></param>
        public SpritesSequencePresenter(ITriggerController trigger) : base(trigger)
        {
            _animator = _trigger.Target.GetComponent<Animator>();
        }

        public override async void Present()
        {
            base.Present();
            
            if (!_trigger.IsLoop && IsPlaying)
                return;
            
            var stateInfo = _animator.GetCurrentAnimatorStateInfo(0);
            //Debug.Log($"{this}, loop > {stateInfo.loop}");
                
            var open = AnimationStates.Open.ToString().ToLower();
            var idle = AnimationStates.Idle.ToString().ToLower();
            var duration = stateInfo.length;

            if (_trigger.IsLoop)
            {
                _animator.Play(IsPlaying ? idle : open);
                IsPlaying = !IsPlaying;
            }
            else
            {
                _animator.Play(open);
                await Task.Delay(TimeSpan.FromSeconds(duration));
                IsPlaying = false;
                _animator.Play(idle);
            }
        }
    }
}