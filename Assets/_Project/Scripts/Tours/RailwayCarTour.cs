﻿using System.Collections.Generic;
using AdvancedCullingSystem.DynamicCullingCore;
using AdvancedCullingSystem.StaticCullingCore;
using NGS.AdvancedRenderSystem;
using Pointers;
using Tours.Triggers;
using UnityEngine;
using VRTK.Prefabs.Pointers;
using Zenject;

namespace Tour
{
	public class RailwayCarTour : MonoBehaviour
	{
		[Inject] private readonly ApplicationManager _appManager = default;
		[Inject] private readonly TriggersController _triggersController = default;
		[Inject] private readonly VRPointerController _vrPointerController = default;
		[Inject] private readonly BaseAppInstaller.Prefabs _basePrefabs = default;
		[Inject] private readonly RailwayStationContextInstaller.Prefabs _rwStationPrefabs = default;
		
		private Camera _camera = default;

		private void Start()
		{
			Cursor.lockState = CursorLockMode.Locked;
			Cursor.visible = false;

			switch (_appManager.ViewMode)
			{
				case AppEnums.ViewModes.Screen:
					InitScreenMode(); 
					break;
				case AppEnums.ViewModes.VR:
					InitVRMode();
					break;
				default: break;
			}

			var wagonModule = Instantiate(_basePrefabs.WagonsPrefabs.Get(_appManager.SelectedWagonType));
			//DynamicCulling.Instance.AddCamera(_camera);
			
			Debug.Log($"ViewMode > " + _appManager.ViewMode);
			Debug.Log($"WagonsPrefabs > " + _basePrefabs.WagonsPrefabs);
			Debug.Log($"SelectedWagonSection > " + _appManager.SelectedWagonSection);
			Debug.Log($"SelectedWagonType > " + _appManager.SelectedWagonType);
			Debug.Log($"VRPointerController > " + _vrPointerController);
			Debug.Log($"Camera > " + _camera);
		}

		private void InitVRMode()
		{
			//throw new System.NotImplementedException();
		}

		private void InitScreenMode()
		{
			var vrtkSystem = Instantiate(_rwStationPrefabs.VRTKScreenModePrefab, _vrPointerController.transform.parent);
			_vrPointerController.Initialize(vrtkSystem.GetComponentInChildren<PointerFacade>());
			_camera = vrtkSystem.GetComponentInChildren<Camera>();
		}

		private void OnDestroy()
		{
			Cursor.lockState = CursorLockMode.None;
			Cursor.visible = true;

			_triggersController.Destroy();
		}
	}
}

