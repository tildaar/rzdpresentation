﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.HighDefinition;
using UnityEngine.Serialization;

namespace Tours
{
    public class TeleportPoint : MonoBehaviour
    {
        [FormerlySerializedAs("_inaccessiblePoint")]
        [Header("Недоступные с этой позиции точки телепортации")]
        [SerializeField] private TeleportPoint[] _inaccessiblePoints = default;
        
        [Range(0, 1.8f)]
        [SerializeField] private float _headPosition = 1.6f;

        [SerializeField] private bool _onlyScreenMode = false;

        private Collider _collider;

        public IEnumerable<TeleportPoint> InaccessiblePoints => _inaccessiblePoints;

        public float HeadPosition => _headPosition;

        private void Start()
        {
            _collider = GetComponent<Collider>();
        }

        public void ToggleCollider(bool value)
        {
            _collider.enabled = value;
        }

        public void PlayAnimation()
        {
            //Debug.Log($"PlayAnimation {_collider.name}");
        }

        public void StopAnimation()
        {
            //Debug.Log($"StopAnimation {_collider.name}");
        }
    }
}
