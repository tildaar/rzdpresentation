﻿using Events;
using Pointers;
using System.Linq;
using UnityEngine;

namespace Tours.ScreenMode
{
    public class PlayerTeleporter : MonoBehaviour, IPointersHandler
    {
        private const float speed = 2f;

        [SerializeField] private GameObject _playerAvatar = default;

        [SerializeField] private AnimationCurve _decelerationСurve = default;

        private TeleportPoint _currentPoint;
        private bool _teleportInProcess;

        private void Start()
        {
            AddPointersListeners();
        }

        private void OnDestroy()
        {
            RemovePointersListeners();
        }

        public void RemovePointersListeners()
        {
            Messenger.RemoveListener<GameObject>(ApplicationEvents.TELEPORT_POINTER_ACTION, PointerActionHandler);
            Messenger.RemoveListener<GameObject>(ApplicationEvents.TELEPORT_POINTER_ENTER, PointerEnterHandler);
            Messenger.RemoveListener<GameObject>(ApplicationEvents.TELEPORT_POINTER_EXIT, PointerExitHandler);
        }

        public void AddPointersListeners()
        {
            Messenger.AddListener<GameObject>(ApplicationEvents.TELEPORT_POINTER_ACTION, PointerActionHandler);
            Messenger.AddListener<GameObject>(ApplicationEvents.TELEPORT_POINTER_ENTER, PointerEnterHandler);
            Messenger.AddListener<GameObject>(ApplicationEvents.TELEPORT_POINTER_EXIT, PointerExitHandler);
        }

        public void PointerActionHandler(GameObject target)
        {
            if (_teleportInProcess)
                return;

            var targetPoint = target.GetComponent<TeleportPoint>();
            if (_currentPoint == null)
                _currentPoint = FindObjectOfType<WagonInitializer>().StartTeleportPoint;
            
            if(_currentPoint.InaccessiblePoints.Any(p => p.Equals(targetPoint)))
                return;

            _teleportInProcess = true;
            var targetPos = target.transform.localPosition;
            var destination = new Vector3(targetPos.x, targetPos.y + targetPoint.HeadPosition, targetPos.z);
            var distance = Vector3.Distance(_playerAvatar.transform.localPosition, destination);
            var coeff = _decelerationСurve.Evaluate(distance);
            //Debug.Log($"dist > {distance}, coeff > {coeff}");
            var t = distance / (speed * coeff);
            
            LeanTween.moveLocal(_playerAvatar, destination, t)
                .setOnComplete(() =>
                {
                    _teleportInProcess = false;
                    _currentPoint = targetPoint;
                });
        }

        public void PointerEnterHandler(GameObject target)
        {
            if (_teleportInProcess)
                return;

            var teleportPoint = target.GetComponent<TeleportPoint>();
            teleportPoint.PlayAnimation();
        }

        public void PointerExitHandler(GameObject target)
        {
            if (_teleportInProcess)
                return;

            var teleportPoint = target.GetComponent<TeleportPoint>();
            teleportPoint.StopAnimation();
        }
    }
}

