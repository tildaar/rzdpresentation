﻿using System;
using UnityEngine;
using VRTK.Prefabs.CameraRig.SimulatedCameraRig.Input;

namespace Tours.ScreenMode
{
    public class CameraRotationController : MonoBehaviour
    {
        [SerializeField] private Transform _playerAvatar = default;
        [SerializeField] private MouseVector2DAction _mouseAction = default;

        private Vector3 _currentRotation;
        
        private void Start()
        {
            _currentRotation = _playerAvatar.localEulerAngles;
            _mouseAction.ValueChanged.AddListener(ValueChangedhandler);
        }

        private void OnDestroy()
        {
            _mouseAction.ValueChanged.RemoveListener(ValueChangedhandler);
        }

        private void ValueChangedhandler(Vector2 value)
        {
            //_playerAvatar.localEulerAngles = _currentRotation; //TEMP
            //return;//TEMP
            
           //Debug.Log($"rotation x > {_playerAvatar.localEulerAngles.x }");
           var xrot = _playerAvatar.localEulerAngles.x;
           if (xrot > 180) xrot -= 360;
           
           if (xrot > 75)
               _playerAvatar.localEulerAngles = new Vector3(75, _currentRotation.y, _currentRotation.z);
           
           if (xrot < -75)
               _playerAvatar.localEulerAngles = new Vector3(-75, _currentRotation.y, _currentRotation.z);
           
           _currentRotation = _playerAvatar.localEulerAngles;
        }
    }
}