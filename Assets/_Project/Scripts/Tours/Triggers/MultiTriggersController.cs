﻿using System;
using System.Collections.Generic;
using MyBox;

using UnityEngine;
using UnityEngine.Rendering;

namespace Tours.Triggers
{
    /// <summary>
    /// version 0.10
    /// </summary>
    [RequireComponent(typeof(MeshFilter),typeof(MeshRenderer),typeof(MeshCollider))]
    public class MultiTriggersController : MonoBehaviour
    {
        [Header("Целевые анимируемые объекты")]
        [SerializeField]
        private List<TriggerData> _targets = default;
        public List<TriggerData> Targets => _targets;

#if UNITY_EDITOR
        private void OnValidate()
        {
             if(Application.isPlaying)
                 return;
        
             if(gameObject.layer != (int)AppEnums.Layers.InteractableLayer)
                 gameObject.layer = (int)AppEnums.Layers.InteractableLayer;
             
             var mesh = GetComponent<MeshFilter>().sharedMesh;
             var renderer = GetComponent<MeshRenderer>();
             var collider = GetComponent<MeshCollider>();
        
             if (renderer.shadowCastingMode != ShadowCastingMode.Off)
                 renderer.shadowCastingMode = ShadowCastingMode.Off;
             
             if (!mesh.Equals(collider.sharedMesh))
                 collider.sharedMesh = mesh;
        
             if (!collider.convex)
                 collider.convex = true;
        }
#endif
    }

    [Serializable] public class TriggerData : ITriggerController
    {
        [Header("Целевой анимируемый объект")]
        [SerializeField]
        private GameObject _target = default;
        public GameObject Target => _target;
        
        [Header("Режим презентации объекта")]
        [SerializeField]
        private TargetActionTypes _actionType = default;
        public TargetActionTypes ActionType => _actionType;

        #region Alembic settings
        [ConditionalField(nameof(_actionType), false, TargetActionTypes.Alembic)]
        [SerializeField]private TriggerController.TimeRange _directAnimationTimeRange = default;

        [ConditionalField(nameof(_actionType), false, TargetActionTypes.Alembic)]
        [SerializeField]private TriggerController.TimeRange  _reverseAnimationTimeRange = default;

        public TriggerController.TimeRange DirectAnimationTimeRange => _directAnimationTimeRange;
        public TriggerController.TimeRange ReverseAnimationTimeRange => _reverseAnimationTimeRange;

        #region Sprites Sequence Settings
        [ConditionalField(nameof(_actionType), false, TargetActionTypes.SpritesSequence)]
        [SerializeField]private bool _isLoop = default;
        public bool IsLoop => _isLoop;
        #endregion
        #endregion
    }

}

