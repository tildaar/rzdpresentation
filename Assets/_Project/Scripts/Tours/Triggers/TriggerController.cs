﻿using System;
using MyBox;
using UnityEngine;
using UnityEngine.Rendering;

using static AppEnums;

namespace Tours.Triggers
{
    public enum TargetActionTypes
    {
        None,
        Fbx,
        SpritesSequence,
        Alembic,
        ParticleSystem,
        ObjectAppearance,
        Light
    }
    
    /// <summary>
    /// version 0.11
    /// </summary>
    [RequireComponent(typeof(MeshFilter),typeof(MeshRenderer),typeof(MeshCollider))]
    public class TriggerController : MonoBehaviour, ITriggerController
    {
        [Header("Целевой анимируемый объект")]
        [SerializeField]
        private GameObject _target = default;
        public GameObject Target => _target;
        
        [Header("Режим презентации объекта")]
        [SerializeField]
        private TargetActionTypes _actionType = default;
        public TargetActionTypes ActionType => _actionType;

        #region Alembic settings
        [ConditionalField(nameof(_actionType), false, TargetActionTypes.Alembic)]
        [SerializeField]private TimeRange _directAnimationTimeRange = default;

        [ConditionalField(nameof(_actionType), false, TargetActionTypes.Alembic)]
        [SerializeField]private TimeRange  _reverseAnimationTimeRange = default;

        public TimeRange DirectAnimationTimeRange => _directAnimationTimeRange;
        public TimeRange ReverseAnimationTimeRange => _reverseAnimationTimeRange;
        
        [Serializable]public struct TimeRange
        {
            public float Start;
            public float End;
        }
        #endregion

        #region Sprites Sequence Settings
        [ConditionalField(nameof(_actionType), false, TargetActionTypes.SpritesSequence)]
        [SerializeField]private bool _isLoop = default;
        public bool IsLoop => _isLoop;
        #endregion
        
        #region Light Settings
        [ConditionalField(nameof(_actionType), false, TargetActionTypes.Light)]
        [SerializeField]private MeshRenderer _lampGlassRenderer = default;
        public MeshRenderer LampGlassRenderer => _lampGlassRenderer;
        #endregion
        
        
        
#if UNITY_EDITOR
        private void OnValidate()
        {
             if(Application.isPlaying)
                 return;

             if(gameObject.layer != (int)Layers.InteractableLayer)
                 gameObject.layer = (int)Layers.InteractableLayer;
             
             var mesh = GetComponent<MeshFilter>().sharedMesh;
             var renderer = GetComponent<MeshRenderer>();
             var collider = GetComponent<MeshCollider>();

             if (renderer.shadowCastingMode != ShadowCastingMode.Off)
                 renderer.shadowCastingMode = ShadowCastingMode.Off;
             
             if (!mesh.Equals(collider.sharedMesh))
                 collider.sharedMesh = mesh;

             if (!collider.convex)
                 collider.convex = true;
        }
#endif
    }
}

