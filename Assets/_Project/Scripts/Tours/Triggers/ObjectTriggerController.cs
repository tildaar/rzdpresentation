﻿using System;
using MyBox;
using UnityEngine;

namespace Tours.Triggers
{
    /// <summary>
    /// version 0.3
    /// </summary>
    public class ObjectTriggerController : MonoBehaviour, ITriggerController
    {
        [Header("Режим презентации объекта")]
        [SerializeField]
        private TargetActionTypes _actionType = default;
        public TargetActionTypes ActionType => _actionType;
        
        [ConditionalField(nameof(_actionType), false, TargetActionTypes.Alembic)]
        [SerializeField]private TriggerController.TimeRange _directAnimationTimeRange = default;

        [ConditionalField(nameof(_actionType), false, TargetActionTypes.Alembic)]
        [SerializeField]private TriggerController.TimeRange  _reverseAnimationTimeRange = default;

        public TriggerController.TimeRange DirectAnimationTimeRange => _directAnimationTimeRange;
        public TriggerController.TimeRange ReverseAnimationTimeRange => _reverseAnimationTimeRange;
        
        public GameObject Target => gameObject;

        public bool IsLoop { get; }

#if UNITY_EDITOR
        private void OnValidate()
        {
            if(Application.isPlaying)
                return;
            
            var colliders = GetComponentsInChildren<MeshCollider>();
            if (colliders == null || colliders.Length == 0)
                throw new Exception("На меши интерактивных объектов должны быть добавлены коллайдеры!");

            foreach (var collider in colliders)
            {
                if(collider.gameObject.layer != (int)AppEnums.Layers.InteractableLayer)
                    collider.gameObject.layer = (int)AppEnums.Layers.InteractableLayer;
            
                if (!collider.convex) collider.convex = true;
            }
        }
#endif
    }
}