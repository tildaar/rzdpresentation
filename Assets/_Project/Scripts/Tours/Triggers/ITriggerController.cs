﻿using UnityEngine;

namespace Tours.Triggers
{
    /// <summary>
    /// version 0.2
    /// </summary>
    public interface ITriggerController
    {
        TargetActionTypes ActionType { get; }
        
        GameObject Target { get; }
        
        TriggerController.TimeRange DirectAnimationTimeRange { get; }
        
        TriggerController.TimeRange ReverseAnimationTimeRange { get; }

        bool IsLoop { get; }
    }
}