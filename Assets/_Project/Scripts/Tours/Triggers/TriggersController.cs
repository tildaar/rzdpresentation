﻿using Events;
using Pointers;
using System.Collections.Generic;
using Tours.TargetPresenters;
using UnityEngine;

namespace Tours.Triggers
{
	public class TriggersController : IPointersHandler
	{
		private static readonly Dictionary<ITriggerController, AbstractTargetPresenter> _triggersHash = 
			new Dictionary<ITriggerController, AbstractTargetPresenter>();

		internal static readonly int EmissiveExposureWeight = Shader.PropertyToID("_EmissiveExposureWeight");
		private static readonly int Alpha = Shader.PropertyToID("alpha");


		private Material[] _matsOfSelectedTarget;
		private AbstractTargetPresenter _currentPresenter;

		public TriggersController()
		{
			AddPointersListeners();
		}

		public static void AddCurrentTrigger(ITriggerController trigger, AbstractTargetPresenter presenter)
		{
			_triggersHash.Add(trigger, presenter);
		}

		public void AddPointersListeners()
		{
			Messenger.AddListener<GameObject>(ApplicationEvents.POINTER_ACTION, PointerActionHandler);
			Messenger.AddListener<GameObject>(ApplicationEvents.POINTER_ENTER, PointerEnterHandler);
			Messenger.AddListener<GameObject>(ApplicationEvents.POINTER_EXIT, PointerExitHandler);
		}

		public void RemovePointersListeners()
		{
			Messenger.RemoveListener<GameObject>(ApplicationEvents.POINTER_ACTION, PointerActionHandler);
			Messenger.RemoveListener<GameObject>(ApplicationEvents.POINTER_ENTER, PointerEnterHandler);
			Messenger.RemoveListener<GameObject>(ApplicationEvents.POINTER_EXIT, PointerExitHandler);
		}

		public void PointerActionHandler(GameObject target)
		{
			var trigger = target.GetComponent<ITriggerController>() ?? target.GetComponentInParent<ITriggerController>();
			
			//if (trigger is TriggerController)
				//var mesh = target.GetComponent<MeshRenderer>();

			//Debug.Log($"PointerActionHandler / target > {target}");
			
			_triggersHash.TryGetValue(trigger, out _currentPresenter);
			if (_currentPresenter != null)
			{
				_currentPresenter.Present();
				return;
			}
				
			switch (trigger.ActionType)
			{
				case TargetActionTypes.Fbx: _currentPresenter = new FbxPresenter(trigger);
					break;
				case TargetActionTypes.SpritesSequence: _currentPresenter = new SpritesSequencePresenter(trigger);
					break;
				case TargetActionTypes.Alembic: _currentPresenter = new AlembicPresenter(trigger);
					break;
				case TargetActionTypes.ParticleSystem: _currentPresenter = new ParticlesPresenter(trigger);
					break;
				case TargetActionTypes.ObjectAppearance: _currentPresenter = new AppearancePresenter(trigger);
					break;
				case TargetActionTypes.Light: _currentPresenter = new LightPresenter(trigger);
					break;
				case TargetActionTypes.None: break;
				default: break;
			}

			if (_currentPresenter == null)
			{
				Debug.LogError($"Presenter creation failed!");
				return;
			}
				
			_currentPresenter.Present();
			if (!_triggersHash.ContainsKey(trigger))
				_triggersHash.Add(trigger, _currentPresenter);
		}

		public void PointerEnterHandler(GameObject target)
		{
			var trigger = target.GetComponent<ITriggerController>() ?? target.GetComponentInParent<ITriggerController>();
			if (trigger is TriggerController)
			{
				var m = target.GetComponent<MeshRenderer>().material;
				LeanTween.value(.75f, 1f, .4f)
					.setOnUpdate((float val) => m.SetFloat(Alpha, val));
			}
			else
			{
				if(_currentPresenter != null && _currentPresenter.IsPlaying)
					return;
				
				//Debug.Log($"PointerEnterHandler | trigger > {trigger}");
				var renderer = target.GetComponent<MeshRenderer>();
				_matsOfSelectedTarget = renderer.materials;
				foreach (var m in _matsOfSelectedTarget)
				{
					LeanTween.value(renderer.gameObject, 1, 0, .3f)
						.setEaseInCubic()
						.setOnUpdate((float val) =>
						{
							m.SetFloat(EmissiveExposureWeight, val);
						})
						.setOnComplete(() =>
						{
							LeanTween.value(renderer.gameObject, 0, 1, 1.4f)
								.setEaseOutSine()
								.setOnUpdate((float val) =>
								{
									m.SetFloat(EmissiveExposureWeight, val);
								});
						});
				}
			}
		}

		public void PointerExitHandler(GameObject target)
		{
			//Debug.Log($"exit target > {target.name}");
			var trigger = target.GetComponent<ITriggerController>();
			if (!(trigger is TriggerController)) 
				return;
			var m = target.GetComponent<MeshRenderer>().material;
			LeanTween.value(1f, .75f, .4f)
				.setOnUpdate((float val) => m.SetFloat("alpha", val));
		}

		public void Destroy()
		{
			RemovePointersListeners();
		}
	}
}
