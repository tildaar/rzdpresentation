﻿using System;
using UnityEngine;

namespace Tours.Triggers
{
    public class ZeroScaler : MonoBehaviour
    {
        private void Start()
        {
            GetComponent<Transform>().localScale = Vector3.zero;
        }
    }
}