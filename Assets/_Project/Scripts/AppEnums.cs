﻿public class AppEnums
{
    public enum Layers
    {
        InteractableLayer = 9,
        TeleportLayer = 10
    }

    public enum ViewModes
    {
        Screen,
        VR
    }

    public enum WagonsSections
    {
        Coupe,
        OpenType,
        WithSeats,
        Express,
        Concept
    }

    public enum WagonsTypes
    {
        None = -1,
        Modular = 2
    }

    public enum Scenes
    {
        SelectModeScene = 1,
        BaseMenuScene = 2,
        RailwayStationScene = 3
    }

    public enum ApplicationTags
    {
        Untagged,
        ToWagonTourBtn,
        ToWagonSlidesBtn,
        SlidesViewerCloseBtn,
        SlidesViewerPrevBtn,
        SlidesViewerNextBtn
    }
}
