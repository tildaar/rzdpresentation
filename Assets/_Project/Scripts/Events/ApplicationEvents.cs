﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Events
{
    public static class ApplicationEvents
    {
        public const string POINTER_ENTER = "pointerEnter";
        public const string POINTER_EXIT = "pointerExit";
        public const string POINTER_ACTION = "pointerAction";

        public const string TELEPORT_POINTER_ENTER = "teleportPointerEnter";
        public const string TELEPORT_POINTER_EXIT = "teleportPointerExit";
        public const string TELEPORT_POINTER_ACTION = "teleportPointerAction";
    }
}

