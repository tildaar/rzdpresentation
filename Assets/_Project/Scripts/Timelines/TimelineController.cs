﻿//using Controllers.Stages;
using System;
using UnityEngine;
using UnityEngine.Playables;

//using static Controllers.AppController;

namespace Controllers.Timelines
{
	public class TimelineController
	{
		public Action<TimelineController> OnEndAnimation;

		protected readonly PlayableDirector _playableDirector;

		public double Duration { get; private set; }

		public double CurrentTime => _playableDirector.time;

		public PlayState PlayState => _playableDirector.state;

		private int _currenAnimId;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="playableDirector"></param>
		public TimelineController(PlayableDirector playableDirector)
		{
			_playableDirector = playableDirector;
			Duration = _playableDirector.duration;
		}

		public void Play()
		{
			if (_playableDirector.state == PlayState.Playing)
			{
				_playableDirector.Pause();
				LeanTween.cancel(_currenAnimId);
			}

			_playableDirector.Play();
			_currenAnimId = LeanTween.delayedCall((float)_playableDirector.duration, () =>
			{
				_playableDirector.Stop();
				OnEndAnimation?.Invoke(this);
			}).id;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="startTime"></param>
		public void Play(float startTime)
		{
			_playableDirector.time = startTime;
			Play();
		}

		public void Stop()
		{
			LeanTween.cancel(_currenAnimId);
			if(_playableDirector != null)
				_playableDirector.Stop();
		}

		public void Pause()
		{
			LeanTween.cancel(_currenAnimId);
			if(_playableDirector != null)
				_playableDirector.Pause();
		}

		public void Resume()
		{
			if(_playableDirector != null)
				_playableDirector.Resume();
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="startTime"></param>
		/// <param name="endTime"></param>
		/// <param name="callback"></param>
		public void PlayRegion(double startTime, double endTime, Action callback = null)
		{
			if(_playableDirector.state == PlayState.Playing)
			{
				_playableDirector.Pause();
				LeanTween.cancel(_currenAnimId);
			}

			var duration = (float)(endTime - startTime);
			_playableDirector.time = startTime;

			_playableDirector.Play();

			_currenAnimId = LeanTween.delayedCall(duration, () =>
			{
				//Debug.Log($" ---------- PlayRegion ------------ ");
				//Debug.Log($"time = {_playableDirector.time}");
				//Debug.Log($"duration = {_playableDirector.duration}");
				//Debug.Log($" ---------------------------------- ");
				Pause();
				if (callback != null)
					callback.Invoke();
				else
					OnEndAnimation?.Invoke(this);
				
			}).id;
			//Debug.Log("------------------------ ");
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="startTime"></param>
		/// <param name="endTime"></param>
		public void PlayRegionLoop(float startTime, float endTime)
		{
			_playableDirector.time = startTime;
			_playableDirector.Play();

			var duration = (float)(endTime - startTime);
			_currenAnimId = LeanTween.delayedCall(duration, () =>
			{
				Pause();
				PlayRegionLoop(startTime, endTime);
			}).id;
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="time"></param>
		public void SetTime(float time)
		{
			if (time < 0 || time > Duration)
				return;

			_playableDirector.time = time;
		}

		public void Destroy()
		{
			//Debug.Log($"destroyed {this}");
			LeanTween.cancel(_currenAnimId);
		}
	}
}
