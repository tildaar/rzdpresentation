﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BaseMenu
{
    public class IntroScreenScaler : MonoBehaviour
    {
        private void Start()
        {
            var rootCanvasTransform = transform.parent.GetComponent<RectTransform>();
            var scaleValue = rootCanvasTransform.sizeDelta.x / ApplicationManager.DefaultWidth;
            // Debug.Log($"recttransform >>> {rootCanvasTransform.name}");

            var rectTransform = GetComponent<RectTransform>();
            rectTransform.localScale = new Vector3(scaleValue, scaleValue, scaleValue);

            var height = ApplicationManager.DefaultHeight * scaleValue;
            rectTransform.anchoredPosition = new Vector2(rectTransform.anchoredPosition.x, -(ApplicationManager.DefaultHeight * .5f - height * .5f));
        }
    }
}

