﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BaseMenu
{
    public class BakedMenuSections : MonoBehaviour
    {
        [SerializeField]
        private GameObject[] _sections = default;

        [SerializeField]
        private GameObject[] _bakedSections = default;

        private const float fadeTime = 0.35f;
        private const float blur = 2f;
        private const float opacity = 0.099f;
       // private const float brightness = 0.1f;

        private MenuController _menuController;

        private void OnDestroy()
        {
            RemoveListeners();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="menuController"></param>
        public void Initialize(MenuController menuController)
        {
            _menuController = menuController;

            foreach (var item in _bakedSections)
            {
                var m = item.GetComponent<Image>().material;
                m.SetFloat("_Amount", 0f);
                //m.SetFloat("Brightness", 1f);
                m.SetFloat("_multAlpha", 1f);
                item.SetActive(false);
            }

            AddListeners();
        }

        private void AddListeners()
        {
            _menuController.OnMenuSectionShown += MenuSectionShownHandler;
            _menuController.OnMenuSectionHidden += MenuSectionHiddenHandler;
        }

        private void RemoveListeners()
        {
            if (_menuController == null)
                return;

            _menuController.OnMenuSectionShown -= MenuSectionShownHandler;
            _menuController.OnMenuSectionHidden -= MenuSectionHiddenHandler;
        }

        private void MenuSectionHiddenHandler()
        {
            for (var i = 0; i < _sections.Length; i++)
            {
                var section = _sections[i];
                var baked = _bakedSections[i];
                if (!section.activeSelf)
                    FadeIn(section, baked);
            }
        }

        private static void FadeIn(GameObject section, GameObject baked)
        {
            var m = baked.GetComponent<Image>().material;
            LeanTween.value(blur, 0f, fadeTime)
                            .setOnUpdate((float v) => m.SetFloat("_Amount", v));

            //LeanTween.value(brightness, 1f, fadeTime)
             //   .setOnUpdate((float v) => m.SetFloat("Brightness", v));

            LeanTween.value(opacity, 1f, fadeTime)
                .setOnUpdate((float v) => m.SetFloat("_multAlpha", v));

            LeanTween.scale(baked, new Vector3(1f, 1f, 1f), fadeTime)
                .setOnComplete(() =>
                {
                    baked.SetActive(false);
                    section.SetActive(true);
                });
        }

        private void MenuSectionShownHandler(int index)
        {
            for (var i = 0; i < _sections.Length; i++)
            {
                var section = _sections[i];
                var baked = _bakedSections[i];
                if (i != index)
                {
                    if (baked.activeSelf) 
                        continue;
                    
                    section.SetActive(false);
                    baked.SetActive(true);
                    var m = baked.GetComponent<Image>().material;

                    LeanTween.scale(baked, new Vector3(0.85f, 0.85f, 0.85f), fadeTime)
                        .setOnStart(() => baked.transform.localScale = new Vector3(1, 1, 1));

                    LeanTween.value(0, blur, fadeTime)
                        .setOnUpdate((float v) => m.SetFloat("_Amount", v));

                    //LeanTween.value(1f, brightness, fadeTime)
                    //    .setOnUpdate((float v) => m.SetFloat("Brightness", v));

                    LeanTween.value(1f, opacity, fadeTime)
                        .setOnUpdate((float v) => m.SetFloat("_multAlpha", v));
                }
                else
                {
                    if (baked.activeSelf)
                    {
                        FadeIn(section, baked);
                    }
                    else
                    {
                        if(!section.activeSelf)
                            section.SetActive(true);
                    }
                }
            }
            //Debug.Log($"show sections {index}");
        }
    }
}