﻿using Controllers.Timelines;
using Slides;
using Zenject;

using UnityEngine;
using UnityEngine.Playables;

using static AppEnums;
using static Slides.SlidesController;

namespace BaseMenu 
{
	public class MenuSceneInitializer : MonoBehaviour
	{
		[Inject] private readonly ApplicationManager _appManager = default;
		[Inject] private readonly MenuSceneContextInstaller.Prefabs _prefabs = default;

		[SerializeField] private GameObject _vRModeContainer = default;
		[SerializeField] private GameObject _screenModeContainer = default;

		public WagonsTypes CurrentSlidersSet { get; private set; }

		private GameObject _introMenuCanvas;
		private SlidesController _slidesController;
		private TimelineController _introAndMenuTimeline;
		private MenuController _menuController;
		private GameObject _activeContainer;

		#region Unity Native
		private void Awake()
		{
			_vRModeContainer.SetActive(false);
		}

		private void Start()
		{
			Setup();
			AddSlidesListeners();
		}

		private void OnDestroy()
		{
			RemoveSlidesListeners();
		}

		#endregion

		private void Setup()
		{
			_activeContainer = _appManager.ViewMode == ViewModes.Screen ? _screenModeContainer : _vRModeContainer;
			Destroy(_activeContainer.Equals(_screenModeContainer) ? _vRModeContainer : _screenModeContainer);

			if (!_activeContainer.activeSelf)
				_activeContainer.SetActive(true);

			_introMenuCanvas = _activeContainer.GetComponentInChildren<Canvas>().gameObject;
			_menuController = _activeContainer.GetComponentInChildren<MenuController>();
			_menuController.gameObject.SetActive(false);

			var uiPlayableDirector = _introMenuCanvas.GetComponentInChildren<PlayableDirector>();
			_introAndMenuTimeline = new TimelineController(uiPlayableDirector);
			_introAndMenuTimeline.OnEndAnimation += EndIntroHandler;
			_introAndMenuTimeline.Play();

			if (_appManager.ViewMode != ViewModes.Screen) 
				return;
			
			uiPlayableDirector.gameObject.AddComponent<IntroScreenScaler>();
			_menuController.gameObject.AddComponent<UIPointerController>();
		}
		
		private void AddSlidesListeners()
		{
			_appManager.OnSlidesViewerClose += SlidesViewerCloseHandler;
			_appManager.OnSlidesSelected += SlidesSelectedHandler;
		}

		private void RemoveSlidesListeners()
		{
			_appManager.OnSlidesViewerClose -= SlidesViewerCloseHandler;
			_appManager.OnSlidesSelected -= SlidesSelectedHandler;
			
		}

		private void EndIntroHandler(TimelineController timeline)
		{
			_introAndMenuTimeline.OnEndAnimation -= EndIntroHandler;
			_menuController.gameObject.SetActive(true);
			_menuController.Initialize();
		}
		
		#region Slides Handlers
		private void SlidesSelectedHandler(WagonsTypes set)
		{
			LeanTween.alphaCanvas(_introMenuCanvas.GetComponent<CanvasGroup>(), 0, .4f)
				.setOnComplete(() =>
				{
					_introMenuCanvas.GetComponent<Canvas>().enabled = false;
				});

			CurrentSlidersSet = set;
			
			_slidesController = Instantiate(_prefabs.SlidesViewer, _activeContainer.transform)
				.GetComponent<SlidesController>();
		}

		private void SlidesViewerCloseHandler()
		{
			Destroy(_slidesController.gameObject);
			_introMenuCanvas.GetComponent<Canvas>().enabled = true;

			LeanTween.alphaCanvas(_introMenuCanvas.GetComponent<CanvasGroup>(), 1, .4f)
				.setOnComplete(_menuController.AddPointersListeners);
		}
		#endregion
	}
}

