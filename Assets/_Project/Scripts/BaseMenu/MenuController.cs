﻿using Controllers.Timelines;

using Events;

using Pointers;

using System;

using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.UI;
using Zenject;

using static AppEnums;
using static Slides.SlidesController;

namespace BaseMenu
{
    public class MenuController : MonoBehaviour, IPointersHandler
    {
        [Inject] public ApplicationManager AppManager { get; }
        public Action<int> OnMenuSectionShown;
        public Action OnMenuSectionHidden;

        [SerializeField] [Range(0, 1)] private float _menuOpenDelay = 0.5f;

        [SerializeField] [Range(0, 1)] private float _menuCloseDelay = 0.5f;

        [Space(5)] [SerializeField] private AbstractRailwayCarSection[] _menuSections = default;

        private BakedMenuSections _bakedSections;

        private AbstractRailwayCarSection _currentSection;

        private LTDescr _menuOpenTimeout;
        private LTDescr _menuCloseTimeout;

        private bool _pointOverButton;

        private void OnDestroy()
        {
            RemovePointersListeners();
            foreach (var section in _menuSections)
                section.Destroy();
        }

        internal void Initialize()
        {
            _bakedSections = GetComponentInChildren<BakedMenuSections>();
            _bakedSections.Initialize(this);
            //Debug.Log($"Initialize | appManager >>> {AppManager}");
            foreach (var section in _menuSections)
                section.Init(this);

            AddPointersListeners();
        }

        public void AddPointersListeners()
        {
            Messenger.AddListener<GameObject>(ApplicationEvents.POINTER_ACTION, PointerActionHandler);
            Messenger.AddListener<GameObject>(ApplicationEvents.POINTER_ENTER, PointerEnterHandler);
            Messenger.AddListener<GameObject>(ApplicationEvents.POINTER_EXIT, PointerExitHandler);
            //Debug.Log($"has POINTER_ACTION > {Messenger.eventTable.ContainsKey(ApplicationEvents.POINTER_ACTION)}");
        }

        public void RemovePointersListeners()
        {
            if (!Messenger.eventTable.ContainsKey(ApplicationEvents.POINTER_ACTION)) 
                return;
            Messenger.RemoveListener<GameObject>(ApplicationEvents.POINTER_ACTION, PointerActionHandler);
            Messenger.RemoveListener<GameObject>(ApplicationEvents.POINTER_ENTER, PointerEnterHandler);
            Messenger.RemoveListener<GameObject>(ApplicationEvents.POINTER_EXIT, PointerExitHandler);
        }

        #region Pointer Handlers
        public void PointerActionHandler(GameObject target)
        {
            if(_currentSection == null)
                return;
            
            var section = _currentSection.Type;
            var wagonType = (WagonsTypes) int.Parse(target.name);
            
            if (target.CompareTag(ApplicationTags.ToWagonTourBtn.ToString()))
            {
                AppManager.OnRailwayCarSelected?.Invoke(section, wagonType);
            }
            else if (target.CompareTag(ApplicationTags.ToWagonSlidesBtn.ToString()))
            {
                RemovePointersListeners();
                Hide();
                LeanTween.delayedCall(1f, () => AppManager.OnSlidesSelected?.Invoke(wagonType));
                //Debug.Log($"POINTER_ACTION | wagon type > {wagonType}");
            }
        }

        public void PointerEnterHandler(GameObject target)
        {
            var isTargetValid = target.CompareTag(ApplicationTags.ToWagonTourBtn.ToString()) ||
                                target.CompareTag(ApplicationTags.ToWagonSlidesBtn.ToString());
            if (_currentSection != null && isTargetValid)
            {
                //Debug.Log($"Enter | target > {target.name}, tag > {target.tag}");
                _currentSection.FadeBtnIn(target);
                _pointOverButton = true;
                return;
            }
            
            AbstractRailwayCarSection section = null;
            if (int.TryParse(target.name, out var i))
                section = _menuSections[i - 1];
            
            if (_currentSection != null && _currentSection != section && _menuCloseTimeout == null )
            {
                //Debug.Log($"-------- PointerEnterHandler -----------");
                //Debug.Log($"new sections > {sections.Type}");
                //Debug.Log($"----------------------------------------");
                _menuCloseTimeout = LeanTween.delayedCall(_menuCloseDelay, HideSectionMenu);
            }
            
            if (_menuOpenTimeout == null)
            {
                _currentSection = section;
                if(section != null)
                    _menuOpenTimeout = LeanTween.delayedCall(_menuOpenDelay, () => ShowSectionMenu(section));
            }

            if (section != null) 
                return;
            
            //Debug.Log($"-------- PointerEnterHandler -----------");
            foreach (var s in _menuSections)
            {
                if (!s.Opened && !s.MenuInAnimation && !s.MenuOutAnimation) 
                    continue;
                Debug.Log($"sections to hide > {s.Type}");
                OnMenuSectionHidden?.Invoke();
                s.Hide();
            }
            //Debug.Log($"----------------------------------------");
        }

        public void PointerExitHandler(GameObject target)
        {
            var isTargetValid = target.CompareTag(ApplicationTags.ToWagonTourBtn.ToString()) ||
                                target.CompareTag(ApplicationTags.ToWagonSlidesBtn.ToString());
            if (isTargetValid)
            {
                _currentSection.FadeBtnOut(target);
                //Debug.Log($"Pointer Exit | currentSection > {_currentSection.Type}");
                if (_menuCloseTimeout != null)
                {
                    LeanTween.cancel(_menuCloseTimeout.id);
                    _menuCloseTimeout = null;
                }

                _pointOverButton = false;
                 return;
            }

            if (_currentSection == null) 
                return;
            
            if(_menuOpenTimeout != null)
            {
                LeanTween.cancel(_menuOpenTimeout.id);
                _menuOpenTimeout = null;
            }
            //Debug.Log($"-------- PointerExitHandler ------------");
            //Debug.Log($"currentSection > {_currentSection.Type}");
            //Debug.Log($"menuCloseTimeout > {_menuCloseTimeout}");
            //Debug.Log($"menuCloseDelay > {_menuCloseDelay}");
            //Debug.Log($"----------------------------------------");
            if (_menuCloseTimeout == null)
                _menuCloseTimeout = LeanTween.delayedCall(_menuCloseDelay, HideSectionMenu);
        }
        #endregion

        private void Hide()
        {
            if (_currentSection != null)
            {
                OnMenuSectionHidden?.Invoke();
                _currentSection.Hide();
                _currentSection = null;
                _pointOverButton = false;
            }

            if (_menuOpenTimeout != null)
            {
                LeanTween.cancel(_menuOpenTimeout.id);
                _menuOpenTimeout = null;
            }

            if (_menuCloseTimeout == null) 
                return;
            LeanTween.cancel(_menuCloseTimeout.id);
            _menuCloseTimeout = null;
        }
        
        private void ShowSectionMenu(AbstractRailwayCarSection section)
        {
            //Debug.Log($"-------- ShowSectionMenu ------------");
            //Debug.Log($"sections to show > {sections.Type}");
            //Debug.Log($"currentSection > {_currentSection.Type}");
            //Debug.Log($"MenuOutAnimation > {sections.MenuOutAnimation}");
            //Debug.Log($"-------------------------------------");

            if (_currentSection == section)
            {
                if(section.MenuOutAnimation)
                {
                    var t = (float)section.ActiveStateController.Duration - (float)section.ActiveStateController.CurrentTime;
                    LeanTween.delayedCall(t, () =>
                    {
                        if (_currentSection == section)
                            section.Show();
                    });
                }
                else section.Show();
            }
            _menuOpenTimeout = null;
        }

        private void HideSectionMenu()
        {
            if (_pointOverButton)
                return;

            //Debug.Log($"-------- HideSectionMenu ------------");
           // Debug.Log($"currentSection > {_currentSection.Type}");
            foreach (var section in _menuSections)
            {
                //Debug.Log($"sections > {sections.Type}");
                if (section != _currentSection && (section.Opened || section.MenuInAnimation))
                    section.Hide();
            }
            //Debug.Log($"-------------------------------------");
            _menuCloseTimeout = null;
        }

        [Serializable] private class AbstractRailwayCarSection
        {
            [SerializeField] private GameObject _graphicsContainer = default;

            [SerializeField] private GameObject _numberGlow = default;

            [Space(5)] [SerializeField] private WagonsSections _type = default;
            public WagonsSections Type => _type;

            [Space(5)] [SerializeField] private PlayableDirector _activeStateAnimator = default;
            [Space(5)] [SerializeField] private GameObject[] _buttons = default;
            [Space(5)] [SerializeField] private float _openFinishTime = default;
            [SerializeField] private float _closeStartTime = default;

            public TimelineController ActiveStateController { get; private set; }

            public bool MenuInAnimation { get; private set; }
            public bool MenuOutAnimation { get; private set; }
            public bool Opened { get; private set; }

            private ApplicationManager _appManager;
            private MenuController _menuController;

            private LTDescr _fadeTween;
            private RectTransform _rect;

            public void Destroy()
            {
                if (_fadeTween != null)
                    LeanTween.cancel(_fadeTween.id);

                if (ActiveStateController == null) 
                    return;
                
                ActiveStateController.OnEndAnimation -= EndMenuInAnimation;
                ActiveStateController.OnEndAnimation -= EndMenuOutAnimation;
            }

            /// <summary>
            /// 
            /// </summary>
            /// <param name="menuController"></param>
            public void Init(MenuController menuController)
            {
                _menuController = menuController;
                _appManager = menuController.AppManager;
                
                foreach (var item in _buttons)
                    item.AddComponent<UIPointerController>();

                DisableButtons();

                _rect = _numberGlow.GetComponent<RectTransform>();
                _fadeTween = LeanTween.color(_rect, new Color(0, 0, 0, 0), 0);

                //Debug.Log($"appManager > {_appManager}, sections > {Type}");
                _activeStateAnimator.Stop();
                ActiveStateController = new TimelineController(_activeStateAnimator);

                ToggleGraphicsContainer(false);
            }

            public void FadeBtnIn(GameObject target)
            {
                var img = target.GetComponent<Image>();
                img.CrossFadeAlpha(1, 0.25f, true);
            }

            public void FadeBtnOut(GameObject target)
            {
                var img = target.GetComponent<Image>();
                img.CrossFadeAlpha(0, 0.25f, true);
            }

            public void Show()
            {
                if (ActiveStateController == null || MenuInAnimation || Opened)
                    return;

                _menuController.OnMenuSectionShown?.Invoke((int)_type);

                ToggleGraphicsContainer(true);
                _fadeTween = LeanTween.color(_rect, new Color(0.3f, 0.3f, 0.3f, 1f), 0.44f);

                ActiveStateController.OnEndAnimation += EndMenuInAnimation;
                MenuInAnimation = true;
                ActiveStateController.PlayRegion(0, _openFinishTime);
            }

            public void Hide()
            {
                if (ActiveStateController == null || MenuOutAnimation || ActiveStateController.CurrentTime <= 0)
                    return;

                DisableButtons();

                _fadeTween = LeanTween.color(_rect, new Color(0, 0, 0, 0), 0.44f);

                ActiveStateController.OnEndAnimation += EndMenuOutAnimation;
                MenuOutAnimation = true;
                ActiveStateController.PlayRegion(_closeStartTime, ActiveStateController.Duration);
            }

            private void EndMenuInAnimation(TimelineController obj)
            {
                ActiveStateController.OnEndAnimation -= EndMenuInAnimation;
                MenuInAnimation = false;
                Opened = true;

                EnableButtons();
            }

            private void EndMenuOutAnimation(TimelineController obj)
            {
                ActiveStateController.OnEndAnimation -= EndMenuOutAnimation;
                ActiveStateController.Stop();
                MenuOutAnimation = false;
                Opened = false;

                ToggleGraphicsContainer(false);
            }

            private void DisableButtons()
            {
                foreach (var item in _buttons)
                {
                    if (_appManager.ViewMode == ViewModes.Screen)
                    {
                        var img = item.GetComponent<Image>();
                        img.raycastTarget = false;
                    }
                    else
                    {
                        var coll = item.GetComponent<Collider>();
                        coll.enabled = false;
                    }
                }
            }

            private void EnableButtons()
            {
                foreach (var item in _buttons)
                {
                    if (_appManager.ViewMode == ViewModes.Screen)
                    {
                        var img = item.GetComponent<Image>();
                        img.raycastTarget = true;
                    }
                    else
                    {
                        var coll = item.GetComponent<Collider>();
                        coll.enabled = true;
                    }
                }
            }

            private void ToggleGraphicsContainer(bool enable)
            {
                if (_graphicsContainer == null)
                    return;

                _graphicsContainer.SetActive(enable);
                
                if (!enable) 
                    return;
                
                foreach (var item in _buttons)
                {
                    var img = item.GetComponent<Image>();
                    img.CrossFadeAlpha(0, 0, true);
                }
            }
        }
    }
}