﻿using Events;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

using  static AppEnums;

namespace BaseMenu
{
    public class UIPointerController : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
    {
        private GameObject _target;

        public void OnPointerClick(PointerEventData eventData)
        {
            if(!Messenger.eventTable.ContainsKey(ApplicationEvents.POINTER_ACTION))
                return;
            
            var target = eventData.pointerCurrentRaycast.gameObject;
            if (target?.layer != Layers.InteractableLayer.GetHashCode()) 
                return;
            
            _target = target;
            Messenger.Broadcast(ApplicationEvents.POINTER_ACTION, _target);
        }

        public void OnPointerEnter(PointerEventData eventData)
        {
            if(!Messenger.eventTable.ContainsKey(ApplicationEvents.POINTER_ENTER))
                return;
            
            var target = eventData.pointerCurrentRaycast.gameObject;
            if (target?.layer != Layers.InteractableLayer.GetHashCode()) 
                return;
            
            _target = target;
            Messenger.Broadcast(ApplicationEvents.POINTER_ENTER, _target);
        }

        public void OnPointerExit(PointerEventData eventData)
        {
            if (_target == null) 
                return;
            
            if(!Messenger.eventTable.ContainsKey(ApplicationEvents.POINTER_EXIT))
                return;
            
            Messenger.Broadcast(ApplicationEvents.POINTER_EXIT, _target);
            _target = null;
        }
    }
}

