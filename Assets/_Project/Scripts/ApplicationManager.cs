﻿using UnityEngine;
using UnityEngine.SceneManagement;

using System;
using Zenject;

using  static AppEnums;

public class ApplicationManager : MonoBehaviour
{
	public Action<WagonsSections, WagonsTypes> OnRailwayCarSelected;
	public Action<ViewModes> OnViewModeSelected;
	public Action<WagonsTypes> OnSlidesSelected;
	public Action OnSlidesViewerClose;
	public Action CloseSlidesWithKeyboard;
	
	[Inject] private readonly BaseAppInstaller.Prefabs _prefabs = default;

	public const float DefaultWidth = 2048;
	public const float DefaultHeight = 1024;

	internal ViewModes ViewMode { get; private set; } = ViewModes.Screen;

	internal WagonsSections SelectedWagonSection { get; private set; }
	internal WagonsTypes SelectedWagonType { get; private set; }
	
	private Scene _currentScene;
	private Scenes _sceneToLoad;
	
	private GameObject _fpsCounter = default;

	private GameObject _preloader = default;
	
	private bool _isSlidesMode;

	private static void QuitApp()
	{
#if UNITY_EDITOR
		PlayProject.Stop();
#else
		Application.Quit();
#endif
	}
	
	#region Unity Native
	private void Start()
	{
		AddListeners();
		
		_fpsCounter = Instantiate(_prefabs.FPSCounter);
		_fpsCounter.SetActive(false);
		
		ChangeScene(Scenes.SelectModeScene);
		
		//SelectedWagonType = WagonsTypes.Modular; // TEMP
		//ChangeScene(Scenes.RailwayStationScene); // TEMP
	}

	private void OnGUI()
	{
		var e = Event.current;
		if (e.type != EventType.KeyUp)
			return;

		if (e.keyCode == KeyCode.F)
		{
			_fpsCounter.SetActive(!_fpsCounter.activeSelf);
			return;
		}
		
		if (e.keyCode != KeyCode.Escape) 
			return;
		
		var scene = (Scenes)Enum.Parse(typeof(Scenes), _currentScene.name);
		//Debug.Log("OnGUI | scene = " + scene);
		switch (scene)
		{
			case Scenes.RailwayStationScene:
				ChangeScene(Scenes.BaseMenuScene);
				break;
			case Scenes.BaseMenuScene:
				if(!_isSlidesMode)
					QuitApp();
				else
					CloseSlidesWithKeyboard?.Invoke();
				break;
			case Scenes.SelectModeScene:
			default:
				QuitApp();
				break;
		}
	}
#endregion

	private void AddListeners()
	{
		OnRailwayCarSelected += RailwayCarSelectHandler;
		OnViewModeSelected += ViewModeSelectedHandler;
		OnSlidesSelected += SlidesSelectedHandler;
		OnSlidesViewerClose += SlidesClosedHandler;
	}

	private void ViewModeSelectedHandler(ViewModes mode)
	{
		ViewMode = mode;
		ChangeScene(Scenes.BaseMenuScene);
		//ChangeScene(Scenes.RailwayStationScene);
	}

	private void RailwayCarSelectHandler(WagonsSections section, WagonsTypes type)
	{
		if(section != WagonsSections.OpenType)
			return;
		
		SelectedWagonSection = section;
		SelectedWagonType = type;
		
		switch (SelectedWagonType)
		{
			case WagonsTypes.Modular:
				ChangeScene(Scenes.RailwayStationScene);
				break;
			case WagonsTypes.None: break;
			default:
				throw new ArgumentOutOfRangeException();
		}
	}

	private void SlidesSelectedHandler(WagonsTypes obj)
	{
		_isSlidesMode = true;
	}
	
	private void SlidesClosedHandler()
	{
		_isSlidesMode = false;
	}
	
	/// <summary>
	/// 
	/// </summary>
	/// <param name="newScene"></param>
	private void ChangeScene(Scenes newScene)
	{
		//Debug.Log("currentScene = " + _currentScene.name);
		//Debug.Log("GetActiveScene = " + SceneManager.GetActiveScene().name);

		_sceneToLoad = newScene;

		if (_currentScene == SceneManager.GetActiveScene())
		{
			SceneManager.sceneUnloaded += SceneUloadedHandler;
			SceneManager.UnloadSceneAsync(_currentScene);
		}
		else
		{
			LoadScene(newScene);
		}
	}

	private void SceneUloadedHandler(Scene scene)
	{
		SceneManager.sceneUnloaded -= SceneUloadedHandler;
		LoadScene(_sceneToLoad);
	}

	private void LoadScene(Scenes scene)
	{
		Debug.Log("LoadScene = " + scene);

		_preloader = Instantiate(_prefabs.ScenePreloader);
		LeanTween.delayedCall(1f, () =>
		{
			SceneManager.LoadSceneAsync(scene.ToString(), LoadSceneMode.Additive);
			SceneManager.sceneLoaded += SceneLoadedHandler;
		});
	}

	private void SceneLoadedHandler(Scene scene, LoadSceneMode mode)
	{
		SceneManager.sceneLoaded -= SceneLoadedHandler;

		if (_preloader != null && _preloader.transform.root != null)
			RemovePreloader();

		_currentScene = scene;

		SceneManager.SetActiveScene(_currentScene);
		Debug.Log($"loaded scene >>> {_currentScene.name}");
	}

	private void RemovePreloader()
	{
		Destroy(_preloader);
	}
}
